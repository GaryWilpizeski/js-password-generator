# Javascript Password Generator

## Objective

Building a password generator length and character options using JavaScript. Using vanilla JS for functionality, <a href="https://fonts.google.com/" target="_blank">Google Fonts</a> for fonts, and <a href="https://fontawesome.com/" target="_blank">Font Awesome</a> for symbols.

Based on <a href="https://www.youtube.com/watch?v=duNmhKgtcsI" target="_blank">this video tutorial</a> by <a href="https://www.youtube.com/user/TechGuyWeb" target="_blank">Brad Traversy</a>, and <a href="https://www.florin-pop.com/blog/2019/09/100-days-100-projects/" target="_blank">Florin Pop's **100 Days - 100 Projects Challenge**</a>.

## Methods

This will generate a password that is comprised of random characters (including uppercase and lowercase letters, numbers, and symbols) between 4 and 20 characters long, and copy it to the clipboard.

The characters are programatically randomly generated one at a time and appended to a string, which is then displayed in the DOM and able to be copied to the clipboard.

This will also be delpoyed on Netlify here:
<a href="https://gwjspasswordgen.netlify.com/" target="_blank">Link</a>

[![Netlify Status](https://api.netlify.com/api/v1/badges/4e94b47f-f722-48cf-93b4-5db99de0ff81/deploy-status)](https://app.netlify.com/sites/gwjspasswordgen/deploys)
